from hwcounter import Timer
with Timer() as t:

    from time import perf_counter
    


    # LOAD LIBRARIES
    import numpy as np
    #from sklearn.model_selection import train_test_split
    from keras.utils.np_utils import to_categorical
    from keras.models import Sequential
    from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, AvgPool2D, BatchNormalization, Reshape
    from keras.preprocessing.image import ImageDataGenerator
    from keras.callbacks import LearningRateScheduler
    import matplotlib.pyplot as plt

    # GLOBAL VARIABLES
    annealer = LearningRateScheduler(lambda x: 1e-3 * 0.95 ** x, verbose=0)
    styles=[':','-.','--','-',':','-.','--','-',':','-.','--','-']

    # evaluate the deep model on the test dataset
    from tensorflow.keras.datasets import mnist
    from tensorflow.keras.models import load_model

    # load train and test dataset
    def load_dataset():
        # load dataset
        (trainX, trainY), (testX, testY) = mnist.load_data()
        
        # reshape dataset to have a single channel
        trainX = trainX.reshape((trainX.shape[0], 28, 28, 1))
        testX = testX.reshape((testX.shape[0], 28, 28, 1))
        # one hot encode target values
        trainY = to_categorical(trainY)
        testY = to_categorical(testY)
        return trainX, trainY, testX, testY

    def prep_pixels(train, test):
        # convert from integers to floats
        train_norm = train.astype('float32')
        test_norm = test.astype('float32')
        # normalize to range 0-1
        train_norm = train_norm / 255.0
        test_norm = test_norm / 255.0
        # return normalized images
        return train_norm, test_norm

    X_train, Y_train, X_test, Y_test = load_dataset()
    X_train, X_test = prep_pixels(X_train, X_test)

    # Model
    model = Sequential()
    model.add(Conv2D(32,kernel_size=5,activation='relu',input_shape=(28,28,1)))
    model.add(MaxPool2D())
    model.add(Conv2D(64,kernel_size=5,activation='relu'))
    model.add(MaxPool2D())
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(10, activation='softmax'))

    #load weights normal model
    model.load_weights("/home/paula/cnn_prunning/modelos_redes/mnist_models/kaggle_mnist_model.h5")
    
    #load weights for prunned model
    #model.load_weights("/home/paula/cnn_prunning/modelos_redes/mnist_models/prunned_kaggle_mnist.h5")
    #for this you need to also load the prunned model 
    start = perf_counter()
    #predict 
    x_predicted = model.predict(X_test)

    end = perf_counter()
    time = end-start
    
    print("time: ", time, "\n")
print("cycles : ", t.cycles,  "\n")
