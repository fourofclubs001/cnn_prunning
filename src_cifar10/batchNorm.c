#ifndef __BATCHNORM__
#define __BATCHNORM__

#include "main.h"

generic_images batch_normalization_3d(generic_images img, batch_norm_struct *bt){
   
    int n_images_in = bt->dim[0];
    int images_in_size = bt->dim[1];
 
    
    //for each feature map
    for(int i = 0; i <  n_images_in; i++){
        //for each row
        for (int j = 0; j < images_in_size; j++)
        {
            //for each col
            for (int k = 0; k < images_in_size; k++)
            {
                //epsilon = 0.001 
                float trans_x = (img[i][j][k]- bt->mean[i])/sqrt( bt->var[i] + 0.001);
                img[i][j][k] = (bt->gamma[i]* trans_x) + bt->beta[i];
            }
            
        }   
    }
    return img;
}

float* batch_normalization_2d(float* vct, batch_norm_struct *bt){

    int vect_size = bt->dim[0];
    
    //for each feature map
    for(int i = 0; i <  vect_size; i++){
        //epsilon = 0.001 
        float trans_x = (vct[i]- bt->mean[i])/sqrt(bt->var[i] + 0.001);
        vct[i] = (bt->gamma[i]* trans_x) + bt->beta[i];   
    }
    return vct;
}
#endif