#ifndef __LOADIMAGE__
#define __LOADIMAGE__

#include "main.h"


void loadData(data *datos, dataset imgs){
 
    FILE* data = fopen("../data/test_batch.bin", "rb");
    
    if(data == NULL){
        printf("Error al cargar los labels");  
        exit(-1); 
    }
    //printf("Labels y Images cargadas \n"); 
    //Normalization constants
    float mean =120.707;
    float std = 64.15;
    float e = 0.0000001;
    //images
    for (int i = 0; i < 1; i++)
    { //for each image 
        unsigned char label; 
        fread(&label,  1, 1, data);
        (datos[i]).label = label;  
        
        generic_images ims = malloc(3*sizeof(generic_image));
        for(int rgb = 0; rgb < 3; rgb++){
            generic_image im = malloc((height)*sizeof(generic_row)); 
            for (int j = 0; j < height; j++)
            {
                generic_row row = malloc((width)*sizeof(float)); 
                for (int k = 0; (k < width); k++)
                {
                    unsigned char pixel; 
                    fread(&pixel,  1, 1, data);  
                    row[k] = (float) ((pixel-mean)/(std+e));
                }
                im[j] = row; 
            }
            ims[rgb] = im;
        }
        
        imgs[i] = ims; 
        (datos[i]).img = ims;
    }
    
  
}
#endif