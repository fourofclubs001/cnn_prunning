#ifndef __CONV__
#define __CONV__

#include "main.h"

generic_images conv(generic_images input_images, conv_struct *parameters){
    
    // Given a generic image and a conv_struct structure
    // Returns the convolution of the input generic image
    // read parameters
    int n_images_in = parameters->dim_in[0];
    int image_in_size = parameters->dim_in[1];

    int kernel_size = parameters->kernel_size;
    int strides = parameters->strides;

    int n_images_out = parameters->dim_out[0];
    int image_out_size = parameters->dim_out[1];

    
    //add zero-padding around image
    generic_images input_images_padding = create_images(n_images_in, image_in_size+2, 0); 
    
    //for each feature map from prev layer
    for (int i = 0; i < n_images_in; i++){
        //copy image in the center 
        for (int j = 1; j < image_in_size+1; j++){

            for (int k = 1; k < image_in_size+1; k++){
                input_images_padding[i][j][k] = input_images[i][j-1][k-1];
                
            }
        
        }
    }
    image_in_size =   image_in_size+2; 
    input_images = input_images_padding;
    //FREE EL  OTRO VECTOR    

     // Create output image
    generic_images output_images = create_images(n_images_out, image_out_size, 0);
    // Create auxiliar variables
    conv_kernel kernel;
    float sum;
     
     // For each output image
    for(int img_out = 0; img_out < n_images_out; img_out++){
        // For each input image
        for(int img_in = 0; img_in < n_images_in; img_in++){

            // Assign kernel
            kernel = parameters->kernels[img_in][img_out];

            // For each kernel position image by row
            for(int i = 0; i < image_in_size - kernel_size + 1; i += strides){

                // For each kernel position image by column
                for(int j = 0; j < image_in_size - kernel_size + 1; j += strides){

                    // sum of kernel multiplication
                    // is the result of apllying a kernel in one image section
                    sum = 0;

                    // For each kernel row
                    for(int p = 0; p < kernel_size; p++){

                        // For each kernel column
                        for(int q = 0; q < kernel_size; q++){

                            // Add to the sum
                            sum += input_images[img_in][i+p][j+q] * kernel[p][q];
                        }
                    }

                    // Assign sum to output image
                    output_images[img_out][(int)i/strides][(int)j/strides] += sum;
                }
            }
        }
    }
    
    
    // For each output image
    for(int img_out = 0; img_out < n_images_out; img_out++){

        // For each row in output image
        for(int i = 0; i < image_out_size; i++){

            // For each column in output image
            for(int j = 0; j < image_out_size; j++){

                // Add bias
                output_images[img_out][i][j] += parameters->bias[img_out];

                // Apply relu activation function
                output_images[img_out][i][j] = relu(output_images[img_out][i][j]);
            }
        }
    }
    
    
    free_generic_images(input_images, n_images_in, image_in_size);
    return output_images;
}

#endif