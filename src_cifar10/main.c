#include "main.h"
#include <time.h>
#include <unistd.h>

#include <stdint.h>
//#include <x86intrin.h> //all instrinsics

int main(void){
	
	dataset imgs = malloc(sizeof(generic_images)*test_size);
	data *imgs_and_labels = malloc(sizeof(data)*test_size);  
	loadData(imgs_and_labels, imgs); 

	
	// Read file
    FILE* file_ptr = fopen("../modelos_redes/vgg16_model/vgg16_model.txt", "r"); 
    

    // Check file_ptr
    if(file_ptr == NULL){
        printf("Error al cargar la red");  
        exit(-1);
    }
    
    
    // Init file_idx
    long int* file_idx = malloc(sizeof(long int));
    *file_idx = 0;

	net* loaded_net = malloc(sizeof(net)); 
	loadNet(loaded_net, file_ptr, file_idx);
	printf("red file cargada \n");

	//Execute net and measure time
	//measure_rdtsc(loaded_net, imgs);  
    //measure_time_clock(loaded_net, imgs);
    //measure_gettimeofday_linux(loaded_net, imgs); 
    //measure_gettimeprocess_linux(loaded_net, imgs);
    //measure_gettimerealtime_linux(loaded_net, imgs); 

	float* res = executeNet(loaded_net, imgs[0]);
	print_vect(res,10); 

	int n_used = 1; 

	//free_data_set(imgs, imgs_and_labels, n_used, test_size, 1, height);
	//free_net(loaded_net);
	//free(file_idx);
	//free(res);
   
	return 0;
}
