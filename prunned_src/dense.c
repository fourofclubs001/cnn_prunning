#ifndef __DENSE__
#define __DENSE__

#include "main.h"

float* dense(float* in_vect, dense_struct* dense){

    // Given a dense struct and an input vector
    // Return the result of the dense layer for the input vector 
    // with dense struct parameters

    float* res = calloc((dense->dim_out), sizeof(float)); // output vector
    
    // Calculate W*x
    // being W the dense->weights and x the input vector
    // where W m*n, x is m*1 and output n*1

    // for each output
    for (int i = 0; i < (dense->dim_out); i++) //n
    {
        // for each input
        for (int j = 0; j < (dense->dim_in); j++) //m 
        {
            res[i] += (dense->weights[i][j])* in_vect[j]; // matrix multiplication
        }
        
    }

    // Add bias
    for(int i = 0; i < dense->dim_out; i++)
    {
        res[i] += dense->bias[i];
    }

    // Apply activation function
    if((dense->actFunc[0] == 'r') & (dense->actFunc[1] == 'e') 
    & (dense->actFunc[2] == 'l') & (dense->actFunc[3] == 'u')){
        
        //relu case
        for (int i = 0; i < (dense->dim_out); i++)
        {
            res[i] = relu(res[i]); 
        }

    }else{

        //softmax case
        softmax(res, (dense->dim_out)); 
    }

    free(in_vect);
    
    return res; 
}

float relu(float x){

    // Given a float value
    // Return 0 if it is less than 0, otherwise returns the value
    if(x < 0){
        return 0;  
    }else{
        return x; 
    }
}

void softmax(float* x, uint8_t dim_x){

    // Given a float vector
    // Returns its softmax result

    float suma = 0; 
    for (int i = 0; i < dim_x; i++)
    {
        suma += exp(x[i]); 
    }
    for (int i = 0; i < dim_x; i++)
    {
        x[i] = exp(x[i]) / suma ;  
    }
     
}

#endif